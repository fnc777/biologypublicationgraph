<?php
    $database_name = "dd";
    $db_connect_statement = "mysql:dbname=" . $database_name . ";host=127.0.0.1;port=3306";
    $pdo = new PDO($db_connect_statement, "root", "");

    $rows = $pdo->query("SELECT bcd.drupal_uid, bcd.`lastname`
                        FROM biblio_contributor bc, biblio_contributor_data bcd
                        WHERE bc.cid = bcd.cid
                        AND bcd.`drupal_uid` IS NOT NULL
                        GROUP BY bc.cid"
                        );


    $json = array();
    foreach ($rows as $row){
        $id = $row["drupal_uid"];
        $adj = $pdo->query("SELECT GROUP_CONCAT(bcd2.drupal_uid) as adj
                            FROM biblio_contributor bc, biblio_contributor_data bcd, biblio_contributor bc2, biblio_contributor_data bcd2
                            WHERE bc.cid = bcd.cid
                            AND bcd.`drupal_uid` = $id
                            AND bc.nid = bc2.nid
                            AND bc.cid <> bc2.cid
                            AND bc2.cid = bcd2.cid"
                            );
        $adjArr = array();
        foreach ($adj as $a){
            foreach ($a as $b){
                if ($b != null){
                    array_push($adjArr, $b);
                }
            }
        }
        $adjArr = array_unique($adjArr);

        $works = $pdo->query("SELECT n.title as title, b.biblio_secondary_title as journal, b.biblio_year as year 
                            FROM biblio_contributor bc, biblio_contributor_data bcd, node n, biblio b
                            WHERE bcd.drupal_uid = $id
                            AND bc.cid = bcd.cid
                            AND bc.nid = n.nid
                            AND b.nid = bc.nid"
                            );
        
        $worksArr = array();
        foreach($works as $work){
            array_push($worksArr, array("title" => $work['title'], "journal" => $work['journal'], 'year' => $work['year']));
        }

        $jsonRow = array('id' => $id, 'name' => $row['lastname'], 'adjacencies' => $adjArr, 'data' => $worksArr);     
        array_push($json, $jsonRow);
    }
    
    echo json_encode($json, JSON_PRETTY_PRINT);
    file_put_contents("data2.JSON", json_encode($json, JSON_PRETTY_PRINT));

?>